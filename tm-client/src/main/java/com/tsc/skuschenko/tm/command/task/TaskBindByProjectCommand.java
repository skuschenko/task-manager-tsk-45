package com.tsc.skuschenko.tm.command.task;

import com.tsc.skuschenko.tm.endpoint.Session;
import com.tsc.skuschenko.tm.endpoint.Task;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import com.tsc.skuschenko.tm.exception.entity.task.TaskNotFoundException;
import com.tsc.skuschenko.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public final class TaskBindByProjectCommand extends AbstractTaskCommand {

    @NotNull
    private static final String DESCRIPTION = "bind task by project";

    @NotNull
    private static final String NAME = "task-bind-by-project";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getSession();
        Optional.ofNullable(session).orElseThrow(AccessForbiddenException::new);
        showOperationInfo(NAME);
        showParameterInfo("project id");
        @NotNull final String projectId = TerminalUtil.nextLine();
        showParameterInfo("task id");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @Nullable Task task = serviceLocator.getTaskEndpoint()
                .bindTaskByProject(
                        session, projectId, taskId);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        showTask(task);
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }
}