package com.tsc.skuschenko.tm.enumerated;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

@Getter
@AllArgsConstructor
public enum Role {

    ADMIN("Administrator"),

    USER("User");

    @NotNull
    public final String displayName;

}
