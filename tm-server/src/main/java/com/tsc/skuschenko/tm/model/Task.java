package com.tsc.skuschenko.tm.model;

import com.tsc.skuschenko.tm.api.entity.IWBS;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Cacheable;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Table(name = "tm_task")
public final class Task extends AbstractBusinessEntity implements IWBS {

    @ManyToOne
    @Nullable
    private Project project;

}
